package threads.thor.bt.kad.tasks;

enum CountedStat {
    SENT,
    RECEIVED,
    STALLED,
    FAILED,
    SENT_SINCE_RECEIVE
}
