package threads.thor.bt.net.portmapping;

public enum PortMapProtocol {

    /**
     * @since 1.8
     */
    TCP,

    /**
     * @since 1.8
     */
    UDP
}
