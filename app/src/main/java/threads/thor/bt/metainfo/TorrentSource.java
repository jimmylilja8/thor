package threads.thor.bt.metainfo;

public interface TorrentSource {

    byte[] getExchangedMetadata();

}
