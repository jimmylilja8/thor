package threads.thor.bt.processor;

public interface ContextFinalizer {

    void finalizeContext(MagnetContext context);
}
