package threads.thor.bt.protocol;

public enum BitOrder {

    /**
     * @since 1.7
     */
    BIG_ENDIAN,

    /**
     * @since 1.7
     */
    LITTLE_ENDIAN
}
